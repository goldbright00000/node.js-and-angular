define({
  "name": "SpurtCommerce V2.1.0",
  "version": "2.1.0",
  "description": "SpurtCommerce RESTFUL API Document",
  "title": "SpurtCommerce RESTFUL API Document",
  "url": "http://localhost:8000",
  "sampleUrl": "http://localhost:8000",
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-06-03T03:05:38.185Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
